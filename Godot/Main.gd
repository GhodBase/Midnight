extends Spatial

export (PackedScene) var loadScene
var grenadeScene = preload("res://Tools/Granade/Granade.tscn")
var stickScene = preload("res://Tools/Stick/Stick.tscn")
var map
var grenade
var stick

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	map = loadScene.instance()
	add_child(map)
	map.find_node("Goal").connect("stopTimer", $GUI/TimeDisplay, "_on_stop_timer")
	map.find_node("BallSpawn").connect("startTimer", $GUI/TimeDisplay, "_on_start_timer")
	$Character.global_translate(map.find_node('SpawnPoint').global_transform.origin)
	$Character.rotation = map.find_node('SpawnPoint').rotation
	$Ball.global_translate(map.find_node('BallSpawn').global_transform.origin)
	$Ball.connect("offField", self, "_restart")
	$Character.connect("offField", self, "_restart")
	$Character.connect("grenadePickedUp", self, "_removeGrenade")
	$Character.connect("stickPickedUp", self, "_removeStick")
	
	var gSpawn = map.find_node("GrenadeSpawn")
	grenade = grenadeScene.instance()
	#Prevent start of timer, so grenade doesn't explode on the ground....
	grenade.find_node("Timer").autostart = false
	grenade.transform.origin = gSpawn.get_transform().origin
	grenade.transform.origin.y += 12
	##### Odd?!?!? If this is not done the grenade spawns of the map?!??!!?
	grenade.transform.origin.z += 40
	#Increase size of grenade for visibility
	grenade.scale *= 10
	gSpawn.add_child(grenade)
	
	var sSpawn = map.find_node("StickSpawn")
	stick = stickScene.instance()
	stick.transform = sSpawn.get_transform()
	stick.transform.origin.y += 2
	##### Odd?!?!? If this is not done the grenade spawns of the map?!??!!?
	stick.transform.origin.z += 30
	stick.transform.origin.x += 5
	#Increase size of stick for visibility
	stick.scale *= 12
	sSpawn.add_child(stick)

func _restart():
	get_tree().reload_current_scene()
	#print(str(map.find_node('SpawnPoint').global_transform.origin))
	#$Character.global_translate(map.find_node('SpawnPoint').global_transform.origin)
	#$Ball.global_translate(map.find_node('BallSpawn').global_transform.origin)
	
func _removeGrenade():
	map.find_node("GrenadeSpawn").remove_child(grenade)
	
func _removeStick():
	map.find_node("StickSpawn").remove_child(stick)