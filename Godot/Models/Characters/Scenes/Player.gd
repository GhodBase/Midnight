extends KinematicBody

var direction = Vector3()
var gravity = -9.8
var velocity = Vector3()
var camera
var character

const SPEED = 6
const ACCELERATION = 3
const DE_ACCELERATION = 5

func _ready():
	camera = get_node("../Camera").get_global_transform()
	character = get_node(".")

func _physics_process(delta):
	var dir = Vector3()
	if Input.is_action_pressed("ui_left"):
		dir += -camera.basis[0]
	if Input.is_action_pressed("ui_right"):
		dir += camera.basis[0]
	if Input.is_action_pressed("ui_up"):
		dir += -camera.basis[2]
	if Input.is_action_pressed("ui_down"):
		dir += camera.basis[2]
		
	dir.y = 0
	dir = dir.normalized()
	velocity.y += gravity * delta
	
	direction = direction * SPEED * delta
	var hv = velocity
	hv.y = 0
	
	var newPos = dir*SPEED
	var accel = DE_ACCELERATION
	
	if(dir.dot(hv)>0):
		accel = ACCELERATION
		
	hv = hv.linear_interpolate(newPos, accel*delta)
	
	velocity.x = hv.x
	velocity.z = hv.z
	
	velocity = move_and_slide(velocity, Vector3(0,1,0))
	var angle = atan2(hv.x, hv.z)
	var char_rot = character.get_rotation()
	char_rot.y = angle
	character.set_rotation(char_rot)
	var speed = hv.length() / SPEED
	
	if is_on_floor() and Input.is_key_pressed(KEY_SPACE):
		velocity.y = 5