extends KinematicBody

export var speed = 600
var direction = Vector3()
var strafeDirection = Vector3()
var movement = 0
var rotate = 0
var straff = 0
var pickUpTool
export var rotateSpeed = 0.4
export var gravity = -30
var velocity = Vector3()
var jumping = false
export var jumpSpeed = 10
var attacking = false
enum TOOLS {none, hitting, throwing, shooting}
var currentTool = TOOLS.shooting
signal offField
signal grenadePickedUp
signal stickPickedUp

onready var rightHand = $HandRight

var granadeScene = preload("res://Tools/Granade/Granade.tscn")
var stickScene = preload("res://Tools/Stick/Stick.tscn")
var uziScene = preload("res://Tools/Uzi/Uzi.tscn")
var uzi

func _ready():
	$Armature/RayCast.add_exception(self)
	if currentTool == TOOLS.shooting:
		load_uzi()
	
func _input(event):
	rotate = 0
	if event is InputEventMouseMotion:
		rotate -= event.relative.x
	
	if event is InputEventKey:
		if event.is_action_pressed("ui_cancel"):
			get_tree().quit()
		
		if event.is_action_pressed("move_forward"):
			movement += 1
		if event.is_action_released("move_forward"):
			movement -= 1
		if event.is_action_pressed("move_backword"):
			movement -= 1
		if event.is_action_released("move_backword"):
			movement += 1
		if event.is_action_pressed("turn_left"):
			straff += 1
		if event.is_action_released("turn_left"):
			straff -= 1
		if event.is_action_pressed("turn_right"):
			straff -= 1
		if event.is_action_released("turn_right"):
			straff += 1
		if event.is_action_pressed("jump"):
			jumping = true
		if event.is_action_pressed("pickup"):
			print("Picking up")
			if pickUpTool:
				print(pickUpTool)
				currentTool = pickUpTool
				rightHand.remove_child(uzi)
				if currentTool == TOOLS.throwing:
					pickUpGrenade()
				if currentTool == TOOLS.hitting:
					pickUpStick()
		if event.is_action_pressed("attack"):
			if currentTool == shooting:
				uzi.get_node("Fire").play()
			attacking = true
		if event.is_action_released("attack"):
			if currentTool == shooting:
				uzi.get_node("Fire").stop()
			attacking = false
	movement = clamp(movement, -1, 1)
	# rotate = clamp(rotate, -1, 1)

func _physics_process(delta):
	# Hit the ball
	if attacking:
		_attack()
	
	# Rotation on character
	rotate_y(rotate * rotateSpeed * delta)
	
	# Calculate direction to move
	direction = Vector3()
	direction += get_global_transform().basis.z
	strafeDirection = Vector3()
	strafeDirection += get_global_transform().basis.x
	
	direction = direction.normalized()
	direction = direction * movement * speed * delta
	strafeDirection = strafeDirection.normalized()
	strafeDirection = strafeDirection * straff * speed * delta
	
	if jumping and is_on_floor():
		velocity.y += jumpSpeed
	
	velocity.x = direction.x + strafeDirection.x
	velocity.z = direction.z + strafeDirection.z
	velocity.y += gravity * delta
	
	if !is_on_floor():
		velocity.y += gravity*delta
	if velocity.y < -80:
		emit_signal("offField")
		
	velocity = move_and_slide(velocity, Vector3(0, 1, 0))
	jumping = false
	attacking = false
	
	rotate = 0

# Would be better to put these in separate gd files and use inheritance
func _attack():
	if currentTool == TOOLS.hitting:
		hitting()
	elif currentTool == TOOLS.throwing:
		throwing()
	elif currentTool == TOOLS.shooting:
		shooting(uzi)
	
	
func hitting():
	var hitBall = $Armature/RayCast
	if hitBall.is_colliding():
		var collider = hitBall.get_collider()
		if collider.is_in_group("ball") and attacking:
			var angleToBall = collider.transform.origin - transform.origin
			collider.apply_impulse(Vector3(), angleToBall.normalized() * 15)
			print("Play ball sound")
			stickScene.instance().find_node("BallHit").play()

func throwing():
	var throwGranade = $Armature/RayCast
	var granade = granadeScene.instance()
	granade.transform = transform
	granade.translate(Vector3(0,0.2,0.3))
	var throwAngle = transform.basis.z.normalized()
	print(String(throwAngle))
	granade.apply_impulse(Vector3(), throwAngle * 3)
	var toolsNode = get_tree().get_root().get_node("Main").find_node("Tools")
	toolsNode.add_child(granade)

func load_uzi():
	uzi = uziScene.instance()
	uzi.transform = rightHand.transform
	rightHand.add_child(uzi)

func shooting(weapon):
	if weapon == uzi:
		uzi.Fire()
	else:
		print("Not uzi")

func _on_Area_body_entered( body ):
	print("I can pick up!")
	if body.name == "Granade":
		pickUpTool = TOOLS.throwing
	if body.name == "Stick":
		pickUpTool = TOOLS.hitting
		
func _on_Area_body_exited( body ):
	print("I can't pick up!")
	pickUpTool = TYPE_NIL

func pickUpGrenade():
	var handGrenade = granadeScene.instance()
	handGrenade.find_node("Timer").autostart = false
	#WHY does this not work!??!??!?!?!?
	handGrenade.transform.origin = rightHand.transform.origin
	rightHand.add_child(handGrenade)
	emit_signal("grenadePickedUp")
	
func pickUpStick():
	var handStick = stickScene.instance()
	handStick.transform.origin = rightHand.transform.origin
	rightHand.add_child(handStick)
	emit_signal("stickPickedUp")