extends Camera

export(NodePath) var nodeToFollow
export var stationaryCamera = false
export var offsetCamera = Vector2()

export var tiltWithMouse = false
export var tiltValue = Vector2(90, 90)
export var rotateWithMouse = false

func _physics_process(delta):
	if !nodeToFollow:
		return
	
	var node = get_node(nodeToFollow)
	
	if !stationaryCamera:
		transform = node.transform
		var nodeDirection = node.get_transform().basis.z
		nodeDirection = nodeDirection.normalized()
		var camPosition = Vector3(nodeDirection.x * offsetCamera.x, offsetCamera.y, nodeDirection.z * offsetCamera.x)
		transform.origin = node.get_transform().origin + camPosition

	
	look_at(node.get_transform().origin, Vector3(0,1,0))