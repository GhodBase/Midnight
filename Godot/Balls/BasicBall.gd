extends RigidBody

signal offField

func _ready():
	pass
	
func _physics_process(delta):
	if linear_velocity.y < -20:
		emit_signal("offField")

func _on_BasicBall_body_entered( body ):
	print("Play sound")
	$AudioStreamPlayer3D.play()
