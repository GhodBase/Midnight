extends RigidBody

var ballScene
export var maxDistance = 2
var granadePoffScene = preload("res://Tools/Granade/GranadeEnd.tscn")

func _ready():
	ballScene = get_tree().get_nodes_in_group("ball")[0]
	$BallHit.connect("finished", self, "_do_sound_finished")

var currentPos
func _physics_process(delta):
	if linear_velocity != currentPos:
		currentPos = linear_velocity

func _on_Timer_timeout():
	var distance = transform.origin.distance_to(ballScene.transform.origin)
	var poff = granadePoffScene.instance()
	var toolsNode = get_tree().get_root().get_node("Main").find_node("Tools")
	poff.transform = transform
	toolsNode.add_child(poff)
	#If the ball is hit play sound before freeing object
	if distance < maxDistance:
		var angleToBall = ballScene.transform.origin - transform.origin
		ballScene.apply_impulse(Vector3(), angleToBall.normalized() * 10)
		$BallHit.play()
	else:
		queue_free()

func _do_sound_finished():
	queue_free()