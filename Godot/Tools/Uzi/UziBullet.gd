extends RigidBody

var oldPos = Transform()

func _ready():
	oldPos = global_transform

func _physics_process(delta):
	$RayCast.global_transform = oldPos
	$RayCast.cast_to = $RayCast.transform.origin
	if $RayCast.is_colliding():
		transform.origin = $RayCast.get_collision_point()*1.01
		var collider = $RayCast.get_collider()
		print(collider.name)
		if collider.name == "Ball":
			$AudioStreamPlayer3D.play()
	
	oldPos = global_transform

func _on_Timer_timeout():
	queue_free()