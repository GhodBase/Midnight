extends Spatial

export (PackedScene) var bulletScene = preload("res://Tools/Uzi/UziBullet.tscn")
var bullet

func _ready():
	pass

func Fire():
	var nozzle = $Nozzle
	bullet = bulletScene.instance()
	bullet.transform = nozzle.get_global_transform()
	var nozzleAngle = nozzle.get_global_transform().basis.z.normalized()
	bullet.apply_impulse(Vector3(), nozzleAngle * 10)
	var toolsNode = get_tree().get_root().get_node("Main").find_node("Tools")
	bullet.get_node("Timer").start()
	toolsNode.add_child(bullet)

func _physics_process(delta):
	if bullet:
		pass