extends Spatial

signal stopTimer


func _on_Area_body_entered( body ):
	if body.get_name() == "Ball":
		emit_signal("stopTimer")