extends Label

var time = 0.0
var timerRunning = false
var initStart = false
onready var goal = $"/root/Main".find_node("Goal")

func _ready():
	$"/root/Main/Ball".connect("startTimer", self, "_on_start_timer")

func _process(delta):
	if timerRunning:
		time += delta
	text = str("%1.2f" % time)
	
func _on_start_timer():
	if !initStart:
		timerRunning = true
		initStart = true
	
func _on_stop_timer():
	timerRunning = false